provider "kubernetes" {
  host = "${scaleway_instance_ip.master_ip.address}"
}
