data "scaleway_image" "debian_buster" {
  architecture = "x86_64"
  name_filter = "Debian Buster"
}

data "template_file" "inventory" {
  template = "${file("${path.module}/templates/inventory.ini.tpl")}"

  vars = {
    master = "master ansible_host=${scaleway_instance_server.master.public_ip}"
    workers = "${join("\n", formatlist("%s ansible_host=%s", scaleway_instance_server.workers.*.name, scaleway_instance_server.workers.*.public_ip))}"
  }
}

resource "local_file" "inventory_file" {
  content = "${data.template_file.inventory.rendered}"
  filename = "${path.module}/../inventory.ini"
}

resource "scaleway_instance_server" "master" {
  enable_ipv6 = true
  image       = "${data.scaleway_image.debian_buster.id}"
  name        = "kube-master"
  type        = "DEV1-S"

  tags = ["k8s", "lab"]
}

resource "scaleway_instance_server" "workers" {
  count       = 2
  enable_ipv6 = true
  image       = "${data.scaleway_image.fedora.id}"
  name        = "kube-worker-${count.index}"
  type        = "DEV1-S"

  tags = ["k8s", "lab"]
}
