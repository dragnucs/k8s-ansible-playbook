[masters]
${master}

[workers]
${workers}

[masters:vars]
ansible_user=root

[workers:vars]
ansible_user=root
